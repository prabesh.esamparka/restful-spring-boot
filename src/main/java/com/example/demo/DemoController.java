package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/demo")
public class DemoController {

    @GetMapping("get")
    public ResponseEntity<Demo> getDemo() {
        Demo demo = new Demo();
        demo.setMessage("Hello Message");
        demo.setDescription("This is description");
        return new ResponseEntity<>(demo, HttpStatus.CREATED);
    }

}
